<?php

interface Preview {

    public function find($name, $path, $isRecursive);
    public function export($name, $path, $message);

}




