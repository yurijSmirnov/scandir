<?php

include_once('previewInterface.php');


class Finder implements Preview {

    public $files;

    public function find($name, $path, $isRecursive = false) {

        $files = scandir($path);
        $searchFiles = array();

        foreach ($files as $key => $fileName) {
            if (stripos($fileName, $name) !== false) {
               $searchFiles[$fileName] = $path .'/'. $fileName;
            }
        }

        $this->files = $searchFiles;

    }

    public function export($name, $path, $message) {

        if (!is_dir($path)) {
            mkdir($path);
        }

        $zip = new ZipArchive();
        $archiveName = $path . '/' . $name;

        if (file_exists($archiveName)) {
            unlink($archiveName);
        }

        if ($zip->open($archiveName, ZipArchive::CREATE) !== true) {
            exit("Cannot open <$archiveName>\n");
        }

        foreach($this->files as $fileName => $filePah) {
            $zip->addFile($filePah, $fileName);
        }

        $zip->close();

        echo $message;
    }

}

$our_class = new Finder();
$our_class->find('itransition', 'folder1');
$our_class->export('archive.zip', 'result', 'Your archive was successfully created');